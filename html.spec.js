import { readFileSync } from "fs";
import { resolve } from "path";
import { tokenize, toHTML } from "./html.js";

const gemtext = readFileSync(resolve("./example.gmi"), "utf-8");

test("--body", () => {
  expect(toHTML(gemtext, { body: true })).toMatchInlineSnapshot(`
    "<h1 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">gmi-web</h1>
    <blockquote style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">a bridge between HTML and Gemini</blockquote>
    <a href=\\"https://codeberg.org/talon/gmi-web\\" style=\\"display:block;\\">https://codeberg.org/talon/gmi-web</a>
    <a href=\\"https://www.npmjs.com/package/gmi-web-cli\\" style=\\"display:block;\\">on NPM </a>
    <p style=\\"padding-bottom:20px;\\"></p>
    <h2 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">line-types</h2>
    <p style=\\"padding-bottom:20px;\\"></p>
    <p style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:</p>
    <ul style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">
    <li>paragraphs </li>
    <li>preformatted blocks </li>
    <li>lists</li>
    <li>quotes </li>
    <li>headings </li>
    </ul>
    <p style=\\"padding-bottom:20px;\\"></p>
    <pre title=\\"if you are familiar with markdown it's kinda like that but even simpler\\" style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;overflow-y:auto;\\">
    if you are
       familiar with markdown
    it's        kinda like that


    but even simpler
    </pre>
    <p style=\\"padding-bottom:20px;\\"></p>
    <h3 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">inline media</h3>
    <a href=\\"video.mp4\\" style=\\"display:block;\\">video with title prop</a>
    <a href=\\"image.jpg\\" style=\\"display:block;\\">image with title prop</a>
    <a href=\\"audio.mp3\\" style=\\"display:block;\\">audio with title prop</a>
    <a href=\\"video-with-no-title.mp4\\" style=\\"display:block;\\">video-with-no-title.mp4</a>
    <a href=\\"image-with-no-title.jpg\\" style=\\"display:block;\\">image-with-no-title.jpg</a>
    <a href=\\"audio-with-no-title.mp3\\" style=\\"display:block;\\">audio-with-no-title.mp3</a>"
  `);
  expect(toHTML(gemtext, { body: true, css: "none" })).toMatchInlineSnapshot(`
    "<h1>gmi-web</h1>
    <blockquote>a bridge between HTML and Gemini</blockquote>
    <a href=\\"https://codeberg.org/talon/gmi-web\\">https://codeberg.org/talon/gmi-web</a>
    <a href=\\"https://www.npmjs.com/package/gmi-web-cli\\">on NPM </a>
    <p></p>
    <h2>line-types</h2>
    <p></p>
    <p>gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:</p>
    <ul>
    <li>paragraphs </li>
    <li>preformatted blocks </li>
    <li>lists</li>
    <li>quotes </li>
    <li>headings </li>
    </ul>
    <p></p>
    <pre title=\\"if you are familiar with markdown it's kinda like that but even simpler\\">
    if you are
       familiar with markdown
    it's        kinda like that


    but even simpler
    </pre>
    <p></p>
    <h3>inline media</h3>
    <a href=\\"video.mp4\\">video with title prop</a>
    <a href=\\"image.jpg\\">image with title prop</a>
    <a href=\\"audio.mp3\\">audio with title prop</a>
    <a href=\\"video-with-no-title.mp4\\">video-with-no-title.mp4</a>
    <a href=\\"image-with-no-title.jpg\\">image-with-no-title.jpg</a>
    <a href=\\"audio-with-no-title.mp3\\">audio-with-no-title.mp3</a>"
  `);
});

test("--html en --author anon --description", () => {
  expect(
    toHTML(gemtext, {
      html: "en",
      author: "anon",
      description: true,
    })
  ).toMatchInlineSnapshot(`
    "<!DOCTYPE html>
    <html lang=\\"en\\" dir=\\"ltr\\" style=''>
    <head>
    <meta charset=\\"utf-8\\">
    <meta name=\\"viewport\\" content=\\"width=device-width,initial-scale=1\\">
    <meta name=\\"color-scheme\\" content=\\"dark light\\">
    <style>p,pre,ul,blockquote,h1,h2,h3{margin-top:0;margin-bottom:0;overflow-wrap:break-word;}p:empty{padding-bottom:20px;}a{display:block;}pre{overflow-y:auto;}img,audio,video{display:block;max-width:100%;}body{margin:0 auto;padding:0.5rem;max-width:48rem;hyphens:manual;}p{font-family:georgia, times, serif;font-size:1.25rem;line-height:1.5;text-indent:0rem;}a{font-size:1.25rem;font-style:normal;font-family:georgia, times, serif;line-height:1.5;text-decoration:underline;}pre{padding:1rem;font-family:consolas, monaco, monospace;font-size:1rem;line-height:1;}h1{font-family:avenir, helvetica, arial, sans-serif;font-size:3rem;line-height:1.25;}h2{font-family:avenir, helvetica, arial, sans-serif;font-size:2.25rem;line-height:1.25;}h3{font-family:avenir, helvetica, arial, sans-serif;font-size:1.5rem;line-height:1.25;}ul{padding-left:1rem;list-style-type:circle;font-size:1.25rem;font-family:georgia, times, serif;line-height:1.25;}blockquote{margin-left:0;margin-right:0;padding-left:0.5rem;border-left-width:0.5rem;border-left-style:solid;font-family:georgia, times, serif;font-size:1.25rem;font-style:italic;line-height:1.25;}html,body,h1,h2,h3,p,a,ul,blockquote,pre::selection{color:black;background-color:white;}blockquote{border-color:black;}pre,::selection,a:hover{color:white;background-color:black;}@media (prefers-color-scheme: dark){html,body,h1,h2,h3,p,a,ul,blockquote,pre::selection{color:white;background-color:black;}blockquote{border-color:white;}pre,::selection,a:hover{color:black;background-color:white;}}</style>
    <title>gmi-web</title>
    <meta name=\\"author\\" content=\\"anon\\">
    <meta name=\\"description\\" content=\\"g...\\">
    </head>
    <body>
    <h1>gmi-web</h1>
    <blockquote>a bridge between HTML and Gemini</blockquote>
    <a href=\\"https://codeberg.org/talon/gmi-web\\">https://codeberg.org/talon/gmi-web</a>
    <a href=\\"https://www.npmjs.com/package/gmi-web-cli\\">on NPM </a>
    <p></p>
    <h2>line-types</h2>
    <p></p>
    <p>gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:</p>
    <ul>
    <li>paragraphs </li>
    <li>preformatted blocks </li>
    <li>lists</li>
    <li>quotes </li>
    <li>headings </li>
    </ul>
    <p></p>
    <pre title=\\"if you are familiar with markdown it's kinda like that but even simpler\\">
    if you are
       familiar with markdown
    it's        kinda like that


    but even simpler
    </pre>
    <p></p>
    <h3>inline media</h3>
    <a href=\\"video.mp4\\">video with title prop</a>
    <a href=\\"image.jpg\\">image with title prop</a>
    <a href=\\"audio.mp3\\">audio with title prop</a>
    <a href=\\"video-with-no-title.mp4\\">video-with-no-title.mp4</a>
    <a href=\\"image-with-no-title.jpg\\">image-with-no-title.jpg</a>
    <a href=\\"audio-with-no-title.mp3\\">audio-with-no-title.mp3</a>
    </body>
    </html>
    "
  `);
});

test("--image, --audio, --video", () => {
  expect(
    toHTML(gemtext, {
      body: true,
      image: ["jpg"],
      video: ["mp4"],
      audio: ["mp3"],
    })
  ).toMatchInlineSnapshot(`
    "<h1 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">gmi-web</h1>
    <blockquote style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">a bridge between HTML and Gemini</blockquote>
    <a href=\\"https://codeberg.org/talon/gmi-web\\" style=\\"display:block;\\">https://codeberg.org/talon/gmi-web</a>
    <a href=\\"https://www.npmjs.com/package/gmi-web-cli\\" style=\\"display:block;\\">on NPM </a>
    <p style=\\"padding-bottom:20px;\\"></p>
    <h2 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">line-types</h2>
    <p style=\\"padding-bottom:20px;\\"></p>
    <p style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:</p>
    <ul style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">
    <li>paragraphs </li>
    <li>preformatted blocks </li>
    <li>lists</li>
    <li>quotes </li>
    <li>headings </li>
    </ul>
    <p style=\\"padding-bottom:20px;\\"></p>
    <pre title=\\"if you are familiar with markdown it's kinda like that but even simpler\\" style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;overflow-y:auto;\\">
    if you are
       familiar with markdown
    it's        kinda like that


    but even simpler
    </pre>
    <p style=\\"padding-bottom:20px;\\"></p>
    <h3 style=\\"margin-top:0;margin-bottom:0;overflow-wrap:break-word;\\">inline media</h3>
    <video controls src=\\"video.mp4\\" title=\\"video with title prop\\"></video>
    <img src=\\"image.jpg\\" title=\\"image with title prop\\"></img>
    <audio controls src=\\"audio.mp3\\" title=\\"audio with title prop\\"></audio>
    <video controls src=\\"video-with-no-title.mp4\\"></video>
    <img src=\\"image-with-no-title.jpg\\"></img>
    <audio controls src=\\"audio-with-no-title.mp3\\"></audio>"
  `);
});

test("tokenize", () => {
  expect(tokenize(gemtext)).toMatchInlineSnapshot(`
    Array [
      Object {
        "h1": "gmi-web",
      },
      Object {
        "quote": "a bridge between HTML and Gemini",
      },
      Object {
        "href": "https://codeberg.org/talon/gmi-web",
      },
      Object {
        "href": "https://www.npmjs.com/package/gmi-web-cli",
        "title": "on NPM ",
      },
      Object {
        "text": "",
      },
      Object {
        "h2": "line-types",
      },
      Object {
        "text": "",
      },
      Object {
        "text": "gmi-web makes writing HTML documents as simple as learning the handful of Gemini line-types:",
      },
      Object {
        "li": "paragraphs ",
      },
      Object {
        "li": "preformatted blocks ",
      },
      Object {
        "li": "lists",
      },
      Object {
        "li": "quotes ",
      },
      Object {
        "li": "headings ",
      },
      Object {
        "text": "",
      },
      Object {
        "alt": "if you are familiar with markdown it's kinda like that but even simpler",
        "pre": "\`\`\`if you are familiar with markdown it's kinda like that but even simpler",
      },
      Object {
        "text": "if you are",
      },
      Object {
        "text": "   familiar with markdown",
      },
      Object {
        "text": "it's        kinda like that",
      },
      Object {
        "text": "",
      },
      Object {
        "text": "",
      },
      Object {
        "text": "but even simpler",
      },
      Object {
        "pre": "\`\`\`",
      },
      Object {
        "text": "",
      },
      Object {
        "h3": "inline media",
      },
      Object {
        "href": "video.mp4",
        "title": "video with title prop",
      },
      Object {
        "href": "image.jpg",
        "title": "image with title prop",
      },
      Object {
        "href": "audio.mp3",
        "title": "audio with title prop",
      },
      Object {
        "href": "video-with-no-title.mp4",
      },
      Object {
        "href": "image-with-no-title.jpg",
      },
      Object {
        "href": "audio-with-no-title.mp3",
      },
      Object {
        "text": "",
      },
    ]
  `);
});
