import { load, rootVariables, style } from "./css.js";

test("rootVariables", () => {
  expect(rootVariables(load({ css: "gmi-web.css" }))).toMatchInlineSnapshot(`
    Object {
      "a-decoration": "underline",
      "a-family": "var(--serif)",
      "a-height": "1.5",
      "a-size": "var(--p-size)",
      "a-style": "normal",
      "background": "white",
      "body-width": "48rem",
      "foreground": "black",
      "h1-family": "var(--sans-serif)",
      "h1-height": "1.25",
      "h1-size": "3rem",
      "h2-family": "var(--sans-serif)",
      "h2-height": "1.25",
      "h2-size": "2.25rem",
      "h3-family": "var(--sans-serif)",
      "h3-height": "1.25",
      "h3-size": "1.5rem",
      "hyphens": "manual",
      "mono": "consolas, monaco, monospace",
      "p-family": "var(--serif)",
      "p-height": "1.5",
      "p-indent": "0rem",
      "p-size": "1.25rem",
      "pre-family": "var(--mono)",
      "pre-height": "1",
      "pre-size": "1rem",
      "quote-family": "var(--serif)",
      "quote-height": "1.25",
      "quote-size": "var(--p-size)",
      "quote-style": "italic",
      "sans-serif": "avenir, helvetica, arial, sans-serif",
      "serif": "georgia, times, serif",
      "ul-family": "var(--serif)",
      "ul-height": "1.25",
      "ul-size": "var(--p-size)",
      "ul-style": "circle",
    }
  `);
});

test("style with variable overrides", () => {
  expect(style({ css: "gmi-web.css", foreground: "#137752" }))
    .toMatchInlineSnapshot(`
    "
    <meta name=\\"color-scheme\\" content=\\"dark light\\">
    <style>p,pre,ul,blockquote,h1,h2,h3{margin-top:0;margin-bottom:0;overflow-wrap:break-word;}p:empty{padding-bottom:20px;}a{display:block;}pre{overflow-y:auto;}img,audio,video{display:block;max-width:100%;}body{margin:0 auto;padding:0.5rem;max-width:48rem;hyphens:manual;}p{font-family:georgia, times, serif;font-size:1.25rem;line-height:1.5;text-indent:0rem;}a{font-size:1.25rem;font-style:normal;font-family:georgia, times, serif;line-height:1.5;text-decoration:underline;}pre{padding:1rem;font-family:consolas, monaco, monospace;font-size:1rem;line-height:1;}h1{font-family:avenir, helvetica, arial, sans-serif;font-size:3rem;line-height:1.25;}h2{font-family:avenir, helvetica, arial, sans-serif;font-size:2.25rem;line-height:1.25;}h3{font-family:avenir, helvetica, arial, sans-serif;font-size:1.5rem;line-height:1.25;}ul{padding-left:1rem;list-style-type:circle;font-size:1.25rem;font-family:georgia, times, serif;line-height:1.25;}blockquote{margin-left:0;margin-right:0;padding-left:0.5rem;border-left-width:0.5rem;border-left-style:solid;font-family:georgia, times, serif;font-size:1.25rem;font-style:italic;line-height:1.25;}html,body,h1,h2,h3,p,a,ul,blockquote,pre::selection{color:#137752;background-color:white;}blockquote{border-color:#137752;}pre,::selection,a:hover{color:white;background-color:#137752;}@media (prefers-color-scheme: dark){html,body,h1,h2,h3,p,a,ul,blockquote,pre::selection{color:white;background-color:#137752;}blockquote{border-color:white;}pre,::selection,a:hover{color:#137752;background-color:white;}}</style>"
  `);
});
